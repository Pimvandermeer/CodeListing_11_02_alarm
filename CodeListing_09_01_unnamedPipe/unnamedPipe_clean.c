#include <unistd.h>    // read(), write(), close(), fork(), pipe()
#include <stdio.h>     // printf(), perror()
#include <sys/wait.h>  // wait()
#include <stdlib.h>    // exit()

int main(void) {
  char message[10];
  int pipeFD[2];       // File descriptors
  
  // Create an unnamed pipe:
  if(pipe(pipeFD) == -1) {
    perror("pipe");
    exit(1);
  }
  
  // Fork off a child:
  switch(fork()) {
  case -1:
    perror("fork");
    exit(1);
    
  case 0:  // Child - receive and print message:
    close(pipeFD[1]);              // close pipe's input (write)
    read(pipeFD[0], message, 6);   // read data from pipe's output
    close(pipeFD[0]);              // close pipe's output (read)
    
    printf("Received from parent through pipe: %s\n", message);
    break;
    
  default:  // Parent - send message:
    close(pipeFD[0]);              // close pipe's output
    write(pipeFD[1], "Hello", 6);  // write to pipe's input
    close(pipeFD[1]);              // close pipe's input
    
    wait(NULL);                    // wait for child
  }
  
  return 0;
}

