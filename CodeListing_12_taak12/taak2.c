/*!

 \file taak2.c
 \brief Sample code to demonstrate the correct use of semaphores
 \author Remko Welling (WLGRW) 
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 This program is dependant on running program taak1 and will print 10 times:
 
 Taak2 start KA
 Taak2 stopt KA
 
 Operation
 =========
 Compile the code using "make" and run it using bash script "run.sh".
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Answer the questions given in bash script "run.sh".


 */
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#define MAXLOOP 10

int main()
{
    int i = 0;
    sem_t *pmutex;
    while ((pmutex = sem_open("sem_mutex", 0)) == SEM_FAILED)
    {
        printf("Geen mutex bij Taak2\n");
        usleep(50000);
    }

    for (i = 0; i < MAXLOOP; i++)
    {
        sem_wait(pmutex);
        printf("Taak2 start KA\n");
        usleep(10000);
        printf("Taak2 stopt KA\n");
        sem_post(pmutex);
        usleep(30000);
    }
    sem_close(pmutex);
    return 0;
}