/*!

 \file taak1.c
 \brief Sample code to demonstrate the correct use of semaphores
 \author Remko Welling (WLGRW) 
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 This program will print 10 times:
 
 Taak1 start KA
 Taak1 stopt KA
 
 Operation
 =========
 Compile the code using "make" and run it using bash script "run.sh".
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Answer the questions given in bash script "run.sh".


 */

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>

#define MAXLOOP 10

int main()
{
    int i = 0;
    sem_t *pmutex;
    pmutex = sem_open("sem_mutex", O_CREAT, 0600, 1);
    for (i = 0; i < MAXLOOP; i++)
    {
        sem_wait(pmutex);
        printf("Taak1 start KA\n");
        usleep(10000);
        printf("Taak1 stopt KA\n");
        sem_post(pmutex);
        usleep(13000);
    }
    sem_close(pmutex);
    sem_unlink("sem_mutex");
    return 0;
}