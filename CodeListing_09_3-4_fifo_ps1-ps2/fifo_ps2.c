#include <stdio.h>          // printf()
#include <unistd.h>         // read(), close()
#include <fcntl.h>          // open(), O_* constants

int main(void) {
  int fd;
  char message[20];

  fd = open("FIFO", O_RDONLY);          // Open the FIFO read-only
  read(fd, &message, sizeof(message));  // Read from the FIFO
  close(fd);                            // Close the FIFO
  printf("%s\n", message);              // Print message to screen
  
  return 0;
}
