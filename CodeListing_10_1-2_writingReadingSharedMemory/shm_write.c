#define _XOPEN_SOURCE 500  // ftruncate
#include <sys/mman.h>  // mmap() etc
#include <fcntl.h>     // O_* constants
#include <string.h>    // strcpy()
#include <unistd.h>    // ftruncate(), close()
#include <stdio.h>     // perror()
#include <stdlib.h>    // exit()

int main(int argc, char *argv[]) {
  char message[100];
  
  if(argc == 1) {
    fprintf(stderr, "Syntax: %s <message>\n", argv[0]);
    exit(1);
  }
  strcpy(message, argv[1]);
  size_t len = strlen(message);  // Size (in bytes) of shared-memory object
  
  // (Create and) open the SHM object:
  int fd = shm_open("/mySHM", O_RDWR|O_CREAT, 0600);
  if(fd == -1) {
    perror("shm_write: shm_open");
    exit(1);
  }
  
  // Size the SHM:
  if(ftruncate(fd, len) == -1) {
    perror("shm_write: ftruncate");
    exit(1);
  }
  
  // Map shmAddr to fd (RW, shared, no offset):
  char *shmAddr = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
  if(shmAddr == MAP_FAILED) {
    perror("shm_write: mmap");
    exit(1);
  }
  
  // fd is no longer needed:  
  close(fd);
  
  // Copy message to shmAddr:
  memcpy(shmAddr, message, len);
  
  // Unmap SHM:
  if(munmap(shmAddr, len) == -1) {
    perror("shm_write: munmap");
    exit(1);
  }
  
  // Don't mark SHM for removal - it should survive this program!
  // shm_unlink("/mySHM");
  
  return 0;
}
