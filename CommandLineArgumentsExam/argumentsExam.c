#include <stdio.h>
#include <string.h> // strlen()

// Compile code with "gcc argumentsExam.c -Wall -o argumentsExam"
// Run code with 3 arguments 

int main(int argc, char** argv)
{
   if ((argc > 1) && (strlen(argv[2]) > 2)){

      // Geef de instructie waarmee het >> adres << van de tweede letter van het eerste 
      // argumentop de display wordt afgedrukt.
      printf("Address of 2nd character of the first argument: %p\n", &argv[1][1]);
      
      // Geef de instructie waarmee van  het tweede argument(een filenaam) de extensie op
      // het display wordt afgedrukt (achtereenvolgens:  txt, cpp en jpg).  
      // Maak gebruik van destandaard C-functiestrlen(die het aantal bytes in een string teruggeeft). De oplossingmoet te gebruiken zijn vooralle voorbeelden.
      printf("Extention of the file name in 2rd arguments is: %s\n", &argv[2][strlen(argv[2])-3]);

      return 0;
   }else{
      printf("Not enough arguments or argument 2 < 3 characters. The number of arguments is: %d\n",argc);
      return -1;
   }


   
}
