/*!

 \file FreeRTOS_PriorityInheritance.ino
 \version 15.1
 \author : ExploreEmbedded, Remko Welling (remko.welling@han.nl)
 Website http://www.exploreembedded.com/wiki
 \brief Program to demonstrate the Binary semaphore usage and priority inversion 

This code has been developed and tested on ExploreEmbedded boards.  
We strongly believe that the library works on any of development boards for respective controllers. 
Check this link http://www.exploreembedded.com/wiki for awesome tutorials on 8051,PIC,AVR,ARM,Robotics,RTOS,IOT.
ExploreEmbedded invests substantial time and effort developing open source HW and SW tools, to support consider 
buying the ExploreEmbedded boards.
 
The ExploreEmbedded libraries and examples are licensed under the terms of the new-bsd license(two-clause bsd license).
See also: http://www.opensource.org/licenses/bsd-license.php

EXPLOREEMBEDDED DISCLAIMS ANY KIND OF HARDWARE FAILURE RESULTING OUT OF USAGE OF LIBRARIES, DIRECTLY OR
INDIRECTLY. FILES MAY BE SUBJECT TO CHANGE WITHOUT PRIOR NOTICE. THE REVISION HISTORY CONTAINS THE INFORMATION 
RELATED TO UPDATES.

Permission to use, copy, modify, and distribute this software and its documentation for any purpose
and without fee is hereby granted, provided that this copyright notices appear in all copies 
and that both those copyright notices and this permission notice appear in supporting documentation.

  Further documentation can be found at: http://exploreembedded.com/wiki/Mutex_Semaphore_01%3A_Priority_Inheritance

  # Example
  
  In this example, we will be creating an LPT. LPT will acquire a semaphore and creates an HPT. HPT will preempt LPT as 
  starts running. HPT tries to acquire a semaphore used by LPT and goes to blocked state resulting in priority inversion. 
  Now LPT starts running and requests for the same semaphore. 
  
    1. LPT starts running and acquires the semaphore.
    2. Now HPT is created and it preempts LPT and starts running. It makes the request to acquire the semaphore. Since the semaphore is already with LPT, HPT goes to blocked state.
    3. LPT starts executing again and tries to take the same semaphore. Since the semaphore is not available, it gets blocked.
    4. Finally the scheduler is left out with the idle task and it keeps running. The LPT and HPT will in blocked state until someone releases the semaphore.
 
  https://www.embedded.com/how-to-use-priority-inheritance/
  
*/

#include <Arduino_FreeRTOS.h>   // FreeRTOS library
#include <semphr.h>             // add the FreeRTOS functions for Semaphores (or Flags).

TaskHandle_t LPT_TaskHandle;    // Type by which tasks are referenced, here LPT_TaskHandle
TaskHandle_t HPT_TaskHandle;    // Type by which tasks are referenced, here HPT_TaskHandle

SemaphoreHandle_t binSemaphore_A = NULL;  // declaration of binary semaphore A

// These variables are required to minimize print output and present teh result of the
// priority inversion.
int printLoopFunctionCounter = 0;
const int PRINT_COUNT = 5; 

void setup()
{  
    Serial.begin(9600);
    
    // Wait a maximum of 10s for Serial to come up
    while (!Serial && millis() < 10000)
      ;
    Serial.println(F("In Setup function, Creating Binary Semaphore"));

    vSemaphoreCreateBinary(binSemaphore_A);  /* Create binary semaphore */

    if(binSemaphore_A != NULL){
        Serial.println(F("Creating low priority task"));
        xTaskCreate(LPT_Task, "LPT_Task", 100, NULL, 1, &LPT_TaskHandle);
    }else{
        Serial.println(F("Failed to create Semaphore"));
    }
}

void loop()
{ // Hooked to Idle Task, will run when CPU is Idle
  if(printLoopFunctionCounter < PRINT_COUNT){
    Serial.println(F("Loop function"));
    delay(50);
  }else{
    if(printLoopFunctionCounter%50 == 0){
      Serial.print(F("."));
    }
    delay(50);
  }
  printLoopFunctionCounter++;
}

void printMsg(TaskHandle_t taskhandle,const char *str) {
    Serial.print(F("Priority "));   // Print task priority 
    Serial.print(uxTaskPriorityGet(taskhandle));
    Serial.print(F(" : "));
    Serial.println(str);            // Print user string
}


/*LPT: Low priority task*/
void LPT_Task(void* pvParameters)
{
    printMsg(LPT_TaskHandle,"LPT_Task Acquiring semaphore"); 
    xSemaphoreTake(binSemaphore_A,portMAX_DELAY);

    printMsg(LPT_TaskHandle,"LPT_Task Creating HPT"); 
    xTaskCreate(HPT_Task, "HPT_Task", 100, NULL, 3, &HPT_TaskHandle); 

    printMsg(LPT_TaskHandle,"LPT_Task Trying to take sempahore again");
    xSemaphoreTake(binSemaphore_A,portMAX_DELAY);   

    printMsg(LPT_TaskHandle,"LPT_Task Finally Exiting");
    vTaskDelete(LPT_TaskHandle);
}

/*HPT: High priority task*/
void HPT_Task(void* pvParameters)
{
    printMsg(HPT_TaskHandle,"HPT_Task Trying to Acquire the semaphore");
    xSemaphoreTake(binSemaphore_A, portMAX_DELAY);

    printMsg(HPT_TaskHandle,"HPT_Task Acquired the semaphore");    

    printMsg(HPT_TaskHandle,"HPT_Task Releasing the semaphore");
    xSemaphoreGive(binSemaphore_A);    

    printMsg(HPT_TaskHandle,"HPT_Task About to Exit");
    vTaskDelete(HPT_TaskHandle);
}
