/*!

 \file taakX.c
 \brief Sample code to demonstrate the correct use of semaphores
 \author Remko Welling (WLGRW) 
 \date 24-3-2019
 \version 1.0
 
 Program description
 ===================
 This program will print X "X moet eerst"
 
 Operation
 =========
 Compile the code using "make" and run it using bash script "run.sh".
 
 Assignment:
 -----------
 1 - Study the code and add comments that explain the program.
 2 - Comment the code describing the operation
 3 - Answer the questions given in bash script "run.sh".

 */

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <unistd.h>
#include <fcntl.h>

int main()
{
    int i = 0;
    sem_t *psync;
    psync = sem_open("sem_sync", O_CREAT, 0600, 1);
    printf("X moet eerst\n");
    sem_post(psync);
    sem_close(psync);
    return 0;
}