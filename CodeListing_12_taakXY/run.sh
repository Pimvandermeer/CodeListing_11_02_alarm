#!/bin/bash
#

## \file run.sh
## \brief Sample code to demonstrate the correct use of semaphores
## \author Remko Welling (WLGRW) 
## \date 24-3-2019
## \version 1.0
##
## Program description
## ===================
## This program shows that two processes that are not related can share a single semaphore. 
## task X is creating a semaphore and delivers the inital value while the task Y waits 
## until the semaphore is created. When the semaphore is created it will execute end terminate.
## Using semaphores the order of running programs can be enforced.
## 
## Question: 
## ---------
## Why is the consumer waiting to print the buffer?
## 
## Assignment:
## -----------
## 1 - Study the code of taskX.c, taskY.c and the bash script.
## 2 - Comment the code describing the operation
## 3 - Explain for the class how the program works and why the semaphores do work.

# 1. Starting two indepedent tasks that use the same mutex
echo " "
echo " 1: two indepedent tasks that use the same mutex in right order." 
echo "----------------------------------------------------------------"
read -p "Press [Enter] key to execute..."
./taakX && ./taakY

# 1. Starting two indepedent tasks that use the same mutex
echo " "
echo " 1: two indepedent tasks that use the same mutex in wrong order." 
echo "----------------------------------------------------------------"
read -p "Press [Enter] key to execute..."
./taakY & sleep 1 && ./taakX
